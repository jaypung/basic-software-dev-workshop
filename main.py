import pandas
from sklearn import model_selection
from apyori import apriori

def load_data(filename):
    data = pandas.read_csv(filename)
    return data

def preprocess(data):
    item_list = data.unstack().dropna().unique()
    train, test = model_selection.train_test_split(data, test_size=.10)
    train = train.T.apply(lambda x: x.dropna().tolist()).tolist()
    return train

def train(train_data, min_support=0.0045, min_confidence=0.2, min_lift=3, min_length=2):
    results = list(apriori(train_data, min_support=min_support, min_confidence=min_confidence, min_lift=min_lift, min_length=min_length))
    return results

def visualize(result):
    for rule in results:
        print("-----------------------")
        pair = rule[0]
        components = [i for i in pair]
        print(pair)
        print('Ruler', components[0], '->', components[1])
        print("Support: ", rule[1])
        print("Confidence: ", rule[2][0][2])
        print("Lift: ", rule[2][0][3])

if __name__ == "__main__":
    data = load_data('store_data.csv')
    processed_data = preprocess(data)
    results = train(processed_data, min_support=0.0045, min_confidence=0.2, min_lift=3, min_length=2)
    visualize(results)